package com.earlyjp.conwaysgameoflife;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jearly
 */
public class Grid {

    //private List<List<Integer>> grid = new ArrayList<>();
    private int[][] grid;   // represents the state of the system
    int rows;               // Number of rows
    int cols;               // Number of columns

    // Constructor
    public Grid(int r, int c) {
        rows = r;
        cols = c;
        grid = new int[r][c];

    }

    public Grid(int r, int c, String s) {
        // initialize the grid with a string
        rows = r;
        cols = c;
        grid = new int[r][c];

        int bound = rows * cols;
        if (bound > s.length()) {
            bound = s.length();
        }

        // intialize the cells
        for (int i = 0; i < bound; i++) {
            char digit = s.charAt(i);
            if (digit == '1') {
                grid[(i / cols)][(i % cols)] = 1;
            } else {
                grid[(i / cols)][(i % cols)] = 0;
            }
        }
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    public void set(int i, int j, int val) {
        // Set a call value
        if (isValid(i, j)) {
            // valid assignment
            grid[i][j] = val;
        }
    }

    public Integer get(int i, int j) {
        // Get a cell value
        if (isValid(i, j)) {
            // valid cell
            return grid[i][j];
        } else {
            // invalid cell
            return null;
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                sb.append(grid[i][j]);
            }
        }

        return sb.toString();

    }

    public boolean isValid(int i, int j) {
        // ensure the requested indexes are valid
        return (i >= 0 && i < rows && j >= 0 && j < cols);
    }

    public Integer countNeighbors(int i, int j) {
        if (isValid(i, j)) {

            // count the neighbors, respecting the boundaryies of the array
            int rowStart = Math.max(0, i - 1);
            int rowEnd = Math.min(rows - 1, i + 1);
            int colStart = Math.max(0, j - 1);
            int colEnd = Math.min(cols - 1, j + 1);

            //System.out.println("DEBUG: " + rowStart + ":" + rowEnd + ":" + colStart + ":" + colEnd);
            int sum = 0;
            for (int r = rowStart; r <= rowEnd; r++) {
                for (int c = colStart; c <= colEnd; c++) {
                    // count only adjacent cells                        
                    sum += grid[r][c];

                }

            }
            // subtract current cell
            sum -= grid[i][j];

            //System.out.println("CM:" + i + ":" + j + ":" + sum);
            return sum;

        } else {
            return null;
        }

    }

    public Integer countNeighborsWrap(int i, int j) {
        if (isValid(i, j)) {

            // count the neighbors, but wrap around the borders
            int rowStart = (i - 1 + rows) % rows;
            int rowEnd = (i + 1) % rows;
            int colStart = (j - 1 + cols) % cols;
            int colEnd = (j + 1) % cols;

            //System.out.println("DEBUG: " + rowStart + ":" + rowEnd + ":" + colStart + ":" + colEnd);
            int sum = 0;
            for (int r = rowStart; r != rowEnd + 1; r = (r + 1) % rows) {
                for (int c = colStart; c != colEnd + 1; c = (c + 1) % cols) {
                    sum += grid[r][c];
                }

            }
            // subtract the current cell
            sum -= grid[i][j];

            return sum;

        } else {
            return null;
        }

    }

    public void show() {
        // Display the grid
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                System.out.printf("%1d ", grid[i][j]);
            }
            System.out.println();
        }
    }

    public Grid transition() {
        // transition from current grid to new grid
        // using survival and replication rules

        Grid next = new Grid(this.rows, this.cols);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {

                int nc = this.countNeighbors(i, j);

                if (nc < 2 || nc > 3) {
                    // does not survive
                    next.set(i, j, 0);
                } else if (nc == 3) {
                    // Any cell with three neighbors becomes live
                    next.set(i, j, 1);
                } else {
                    // Has two neighbors, thus survives
                    next.set(i, j, grid[i][j]);
                }

            }

        }

        return next;
    }

}

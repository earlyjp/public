package com.earlyjp.conwaysgameoflife;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

public class FXMLController implements Initializable {

    private Timeline tl;    // Timeline controlling animation
    private int count;      // Frame count
    private Grid current;   // Current cell Grid

    @FXML
    private GridPane gp;
    
    @FXML
    private BorderPane bp;

    @FXML
    private Spinner<Integer> spinner;

    @FXML
    private Button buildButton;

    @FXML
    private Label buildLabel;

    @FXML
    private Button runButton;

    @FXML
    private Button pauseButton;

    @FXML
    private void handleRunButtonAction(ActionEvent event) {
        // Verify Grid is present
        if (current != null) {
            if (!tl.getStatus().equals(Animation.Status.RUNNING)) {
                // disable the Build button
                buildButton.setDisable(true);
                buildLabel.setText("Running...");
                // Disable the Run button
                runButton.setDisable(true);
                // Enable the Pause button
                pauseButton.setDisable(false);
                // start the timeline
                tl.play();
            }
        }

    }

    @FXML
    private void handlePauseButtonAction(ActionEvent event) {

        if (tl.getStatus().equals(Animation.Status.RUNNING)) {
            // Enable the Build button
            buildButton.setDisable(false);
            buildLabel.setText("");
            // Enable the Run button
            runButton.setDisable(false);
            // Disable the Pause button
            pauseButton.setDisable(true);
            // pause the time line  
            tl.pause();
        }
    }

    @FXML
    private void handleBuildButtonAction(ActionEvent event) {
        // Build a Grid
        int gridSize = spinner.getValue();
        current = new Grid(gridSize, gridSize);
        // Build a GridPane
        makeGridPane(gridSize, 500, 500);

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //System.out.println("Initializing...");
        // Disable the Pause button
        pauseButton.setDisable(true);

        // Frame counter
        count = 0;

        // intitialize the Timeline
        tl = new Timeline(
                new KeyFrame(Duration.millis(250),
                        new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent actionEvent) {
                                // Each tick is 250ms
                                //System.out.println("TICK: " + count);
                                count++;
                                // Transform the Grid
                                current = current.transition();
                                // repaint GridPane
                                repaintGridPane(current);
                            }
                        }));
        tl.setCycleCount(Animation.INDEFINITE);

        // Set up a listener for the GridPane
        gp.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                double sceneX = e.getSceneX();
                double sceneY = e.getSceneY();
                double gridX = gp.getBoundsInParent().getMinX();
                double gridY = gp.getBoundsInParent().getMinY();

                double relX = sceneX - gridX;   // Relative X in the GridPane
                double relY = sceneY - gridY;   // Relative Y in the GridPane

                for (Node node : gp.getChildren()) {

                    if (node instanceof Rectangle) {

                        if (node.getBoundsInParent().contains(relX, relY)) {
                            // Found the cell
                            Rectangle r = (Rectangle) node;
                            toggleCell((Rectangle) node);
                            // Update the Grid
                            current = makeGrid(gp);
                            //current.show();
                            repaintGridPane(current);

                        }
                    }
                }
            }
        });

    }

// Build a GridPane from a Grid
    private void makeGridPane(int size, double h, double w) {

        // Clear the GridPane
        gp.getChildren().clear();

        // use the smaller of height and width to define the size of cells
        int space = (int) Math.min(h, w);

        int cellSize = space / size;

        // Popluate the GridPane
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {

                // Create a new Rectangle
                Rectangle cell = new Rectangle();
                cell.setWidth(cellSize);
                cell.setHeight(cellSize);
                cell.setStroke(Color.BLACK);
                cell.setFill(Color.WHITE);

                // Register mouse event
                cell.setMouseTransparent(true);
                
                // Add cell to GridPane
                gp.add(cell, j, i);
            }
        }

    }

    public void toggleCell(Rectangle r) {
        if (r.getFill() == Color.WHITE) {
            r.setFill(Color.GREEN);
        } else {
            r.setFill(Color.WHITE);
        }
    }

    public Grid makeGrid(GridPane gp) {
        // Build a state string
        String state = "";
        int maxCol = 0;
        for (Node n : gp.getChildren()) {
            if (n instanceof Rectangle) {
                Rectangle r = (Rectangle) n;
                if (r.getFill() == Color.GREEN) {
                    // live cell -- append "1"
                    state += "1";
                } else {
                    // dead cell -- append "0"
                    state += "0";
                }
                // update the max column
                if (GridPane.getColumnIndex(n) > maxCol) {
                    maxCol = GridPane.getColumnIndex(n);
                }
            }
        }

        // create the Grid
        int rows = state.length() / (maxCol + 1);
        Grid newGr = new Grid(rows, (maxCol + 1), state);
        return newGr;
    }

    private void repaintGridPane(Grid g) {
        // Iterate over each Rectagle in GridPane
        for (Node n : gp.getChildren()) {
            if (n instanceof Rectangle) {
                Rectangle r = (Rectangle) n;
                int row = GridPane.getRowIndex(n);
                int col = GridPane.getColumnIndex(n);
                // lookup the value
                int val = g.get(row, col);
                if (val == 1) {
                    // live cell
                    r.setFill(Color.GREEN);
                } else {
                    // dead cell
                    r.setFill(Color.WHITE);
                }
            }
        }
    }

}

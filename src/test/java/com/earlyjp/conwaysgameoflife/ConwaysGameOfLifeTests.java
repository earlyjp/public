package com.earlyjp.conwaysgameoflife;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jearly
 */
public class ConwaysGameOfLifeTests {
    
    public ConwaysGameOfLifeTests() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    
    // Test Still Life Patterns

     @Test
     public void blockStillLife() {
         String state = "0000011001100000";
         Grid g = new Grid(4, 4, state);
         g = g.transition();
         // should match orginal state
         assertTrue("Failure - Block Still Life", state.equals(g.toString()));
     }
     
     
     @Test
     public void beehiveStillLife() {
         String state = "000000001100010010001100000000";
         Grid g = new Grid(5, 6, state);
         g = g.transition();
         // should match orginal state
         assertTrue("Failure - Beehive Still Life", state.equals(g.toString()));
     }
     
      @Test
     public void loafStillLife() {
         String state = "000000001100010010001010000100000000";
         Grid g = new Grid(6, 6, state);
         g = g.transition();
         // should match orginal state
         assertTrue("Failure - Loaf Still Life", state.equals(g.toString()));
     }
     
     // Test Oscillator Patterns
     @Test
     public void blinkerOscillator() {
        String state = "0000000000011100000000000";
        String state2 = "0000000100001000010000000";
        Grid g = new Grid(5,5,state);
        g = g.transition();
        assertTrue("Failure - Blinker Oscillator, transiation one", state2.equals(g.toString()));
        g = g.transition();
        assertTrue("Failure - Blinker Oscillator, transiation two", state.equals(g.toString()));
     }
     
     
     @Test
     public void toadOscillator() {
         
        String state = "000000000000001110011100000000000000";
        String state2 = "000000000100010010010010001000000000";
        Grid g = new Grid(6,6,state);
        g = g.transition();
        assertTrue("Failure - Toad Oscillator, transiation one", state2.equals(g.toString()));
        g = g.transition();
        assertTrue("Failure - Toad Oscillator, transiation two", state.equals(g.toString()));
     }
     
      @Test
     public void beaconOscillator() {
         
        String state = "000000011000011000000110000110000000";
        String state2 = "000000011000010000000010000110000000";
        Grid g = new Grid(6,6,state);
        g = g.transition();
        assertTrue("Failure - Beacon Oscillator, transiation one", state2.equals(g.toString()));
        g = g.transition();
        assertTrue("Failure - Beacon Oscillator, transiation two", state.equals(g.toString()));
     }     
     
}
